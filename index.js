// In JS ,classes can be created yung the "class" keyword and {}

/*
		Syntax:
			class <NameOfClass> {
	
			}
		
*/

// class Student {
// 	// constructor method
// 	constructor(name, email,) {
// 		this.name = name;
// 		this.email = email;
// 	}
// }

// let studentOne = new Student('john', 'john@mail.com');
// let studentTwo = new Student();

/*
	Mini ACtivity
		create a new class called Person

		this person shoould be able to instantiate a new object with the ff fields

		name,
		age (should be a number and must be greater that or equal to 18, otherwise set the property to undefined)
		nationality,
		address

		instantiate 2 new objects from the Person class. person 1 and person 2

		Log both object in the console
	
*/


class Person {

	constructor(name, age, nationality, address) {
    	this.name = name;
    	this.age = (typeof age === 'number' && age >= 18 ? age : undefined);
    	this.nationality = nationality;
    	this.address = address;
}

}

let personOne = new Person('Jason', 19, 'Filipino', 'Manila');
let personTwo = new Person('Jerimiah', 16, 'Filipino', 'Batangas');

console.log(personOne);
console.log(personTwo);


/*
	MINI ACTIVITY
	1.	What is the blueprint where objects are created from?
		-class

	2.	What is the naming convention applied to classes?
		-PascalCase

	3.	What keyword do we use to create objects from a class?
		-new

	4.	What is the technical term for creating an object from a class?
		-instantiation 
	
	5.	What class method dictates HOW objects will be created from that class?
		-constructor method

*/

class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.gradeAve = undefined;
    this.passed = undefined
    this.passedWithHonors = undefined
    
    if (grades.length === 4 &&
        grades.every(grade => typeof grade === 'number' && grade >= 0 && grade <= 100)) {
      this.grades = grades;
    } else {
      this.grades = undefined;
    }
  }

  // Methods

  	login() {
  		console.log(`${this.email} has logged in`);
  		return this;
  	}
  	logout() {
  		console.log(`${this.email} has logged out`);
  		return this
  	}
  	listGrades(){
  		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
  		return this;
  	}
  	totalAverage() {
  		let sum = 0;
  		this.grades.forEach(grade => sum = sum + grade);
  		this.gradeAve = sum/4;
  		return this;
  	}
  	willPass() {
  		this.passed = this.totalAverage().gradeAve >= 85 ? true : false;
  		return this;
  	}

  	willPassWithHonors() {
  		if (this.passed) {
  		    if (this.gradeAve >= 90) {
  		        this.passedWithHonors = true;
  		    } else {
  		        this.passedWithHonors = false;
  		    }
  		} else {
  		    this.passedWithHonors = false;
  		}
  		return this;
  	}
  }

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// Getter and Setter
// Best practice dictates that we regulate access 


/*
	1.	Should class methods be included in the class constructor?
		-No

	2.	Can class methods be separated by commas?
		-Yes

	3.	Can we update an object’s properties via dot notation?
		-Yes

	4.	What do you call the methods used to regulate access to an object’s properties?
		-Getter setter

	5.	What does a method need to return in order for it to be chainable?
		-return the Object

*/